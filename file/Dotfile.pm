package Dotfile;
use v5.12;
use FindBin '$Bin';
use File::Find;
use Getopt::Long;
use Pod::Usage;

our $VERSION = "0.3";

our $repo = "$Bin/repo";

sub import {
    my ($mod, @opts) = @_;
    my %o;
    GetOptions(\%o, "help|?", "man", "version", @opts) or pod2usage(2);
    pod2usage(1) if $o{help};
    pod2usage(-exitstatus => 0, -verbose => 2) if $o{man};
    if ($o{version}) {
        say $VERSION;
        exit 0;
    }
    *::opt = \%o;
    return;
}

sub trim ($$) {
    my ($prefix, $path) = @_;
    $path =~ s{^\Q$prefix\E/}{};
    return $path;
}

sub go_home {
    chdir $ENV{HOME} or die "cannot go home to $ENV{HOME} $!\n";
}

sub walk (&;@) {
    go_home();
    my ($fun, @paths) = @_;
    if (@paths) {
        s/^\.// for @paths;
        @paths = map "$repo/$_", @paths;
    }
    else {
        @paths = ("$repo/");
    }
    find {
        wanted => sub {
            my $f = trim($repo, $_);
            return unless $f;
            return if $f =~ m{^\.|/\.};
            $fun->(".$f");
        },
        preprocess => sub { sort @_ },
        no_chdir => 1,
    }, @paths;
}

sub got ($) {
    my ($rf) = @_;
    $rf =~ s/^\.//;
    my @r = stat "$repo/$rf" or return;
    my $hf = ".$rf";
    my @h = stat $hf or return;
    return 1 if ($h[0] == $r[0] and $h[1] == $r[1]
        and $h[3] == $r[3] and $r[3] > 1);
    return "";
}

1;
