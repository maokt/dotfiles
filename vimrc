set nocompatible
set novb
set textwidth=128
set nowrap
set expandtab
set tabstop=4
set shiftwidth=4
set sidescroll=4
set backspace=indent,eol,start
set autoindent
set formatoptions+=norm1
set ignorecase smartcase
set incsearch
set cryptmethod=blowfish
set autowrite
set nobackup
set ruler showcmd
set scrolloff=2
set wildmenu
set pastetoggle=<Insert>
set guioptions-=tT
set clipboard=exclude:.* " don't connect to X11
set encoding=utf-8
set termencoding=utf-8
set background=dark
filetype plugin indent on
syntax enable

augroup vimrc
au BufRead,BufNewFile TODO    set filetype=vo_base
au BufRead,BufNewFile TODO-*  set filetype=vo_base
au BufRead,BufNewFile *.todo  set filetype=vo_base
" .md is markdown, not modula2
au BufNewFile,BufRead *.md    set filetype=markdown
augroup END

let erlang_folding=1
let erlang_skel_replace=0
let erlang_skel_header={"author":"Marty Pauley","owner":"MP"}

