#!/bin/bash
# this may not work on non-bash login shells

umask 002
ulimit -S -c 0

PATH="$PATH":/sbin:/usr/sbin
[ -d ~/bin ] && PATH=~/bin:"$PATH"

export LANG=en_GB.UTF-8
export EDITOR=vim
export VISUAL=$EDITOR
export PAGER=less
export LESS=MRSiqs
export LESSCHARSET=utf-8
[ -x /usr/bin/lessfile ] && eval $(lessfile)

SHORTHOSTNAME=${HOSTNAME%%.*}
[ -f ~/.profile.$SHORTHOSTNAME ] && . ~/.profile.$SHORTHOSTNAME

if [ "$BASH" -a "$PS1" ]
then
    [ -r ~/.bashrc ] && . ~/.bashrc
    [ "$TERM" = "linux" ] && echo -e '\e%GUTF8 mode selected'
fi

